import java.util.Scanner;
import java.nio.file.*;

public class PathConvert {
    public static void main(String[] args) {

        String name;
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the file name >> ");
        name = input.nextLine();
        //name = "data.txt"; // If console isn't working

        Path inputPath = Paths.get(name);

        Path fullPath = inputPath.toAbsolutePath();

        System.out.println("Full Path is " + fullPath.toString());

        input.close();
    }
}