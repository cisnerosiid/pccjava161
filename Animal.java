/**
 * This is an animal class to represent a parent class.
 * 
 * @author Daniel Cisneros II
 * @version 1.0.0
 * @since 2/14/2019
 */
public class Animal {
    protected int weight;
    protected int legs;

    /**
     * The default constructor sets the weight to 10 pounds and the legs to 4.
     */
    public Animal() {
        this.weight = 10;
        this.legs = 4;
    }

    /**
     * The alternative constructor for the animal object.
     * 
     * @param weight Weight is in pounds.
     * @param legs   Legs is in count.
     */
    public Animal(int weight, int legs) {
        this.weight = weight;
        this.legs = legs;
    }

    /**
     * Sets the weight of the animal in pounds.
     * 
     * @param weight Weight is in pounds.
     */
    protected void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * Sets the amount of legs.
     * 
     * @param legs Legs is in count.
     */
    protected void setLegs(int legs) {
        this.legs = legs;
    }

    /**
     * Returns the weight in pounds.
     * 
     * @return The weight in pounds
     */
    public int getWeight() {
        return this.weight;
    }

    /**
     * Returns the amount of legs. 
     * 
     * @return The legs in count. 
     */
    public int getLegs() {
        return this.legs;
    }

    /**
     * Reports the amount of food eaten. 
     * 
     * @param food Food in pounds
     */
    public void eatFood(int food) {
        System.out.println("This method is called from the Animal Class");
        System.out.println("The animal ate " + food + " pounds");
    }

}