/**
 * A driver class to show polymorphism.
 * 
 * @author Daniel Cisneros II
 * @version 1.0
 * @since 2/21/2019
 */
public class Animal2Driver {
    public static void main(String[] args) {
        Animal2 fido;
        
        fido = new Dog2();
        fido.speak();

        fido = new Cow2();
        fido.speak();

        // Dynamic Method Example
        Dog2 dog = new Dog2();
        talkingAnimal(dog);

        Cow2 cow = new Cow2();
        talkingAnimal(cow);

        // Arrays of subclass objects using a super class. 
        Animal2[] animalArray = new Animal2[3];

        animalArray[0] = new Dog2();
        animalArray[1] = new Dog2();
        animalArray[2] = new Dog2();

        animalArray[0].setName("Uno");
        animalArray[1].setName("Dos");
        animalArray[2].setName("Tres");
        
        // For each Animal2 class in my animalArray use animal as the variable. 
        for (Animal2 animal : animalArray) {
            talkingAnimal(animal);
        }

        System.out.println(dog.equals(cow)); // Return true or false

        int myInt = 3;
        int myInt2 = 7;

        if (myInt == myInt2) {
            //statement
        }

        String myStr = "Hello";
        String myStr2 = "GoodBye";

        if (myStr.equals(myStr2)) {
            //statements
        }

        Dog2 james = new Dog2();
        Dog2 brown = new Dog2();

        if (james.equals(brown)) {
            //statements
        }
    }

    public static void talkingAnimal(Animal2 animal) {
        System.out.println("Come one come all!");
        System.out.println("See the amazing talking animal!");
        System.out.println(animal.getName() + " says");
        animal.speak();
        System.out.println("*****************");
    }
}