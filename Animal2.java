/**
 * A superclass to demonstrate dynamic binding. (Parent)
 * @author Daniel Cisneros II
 * @version 1.0
 * @since 2/21/2019
 */
public abstract class Animal2 {
    private String color;
    private String name;

    /**
     * Default Constructor
     * @param color The color of the animal.
     * @param name The name of the animal.
     */
    public Animal2(String color, String name) {
        this.color = color;
        this.name = name;
    }

    /**
     * Secondary Constructor
     */
    public Animal2() {
        this.color = "Brown";
        this.name = "Pet";
    }

    /**
     * The color of the animal.
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * The color of the animal.
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * What the animals says.
     */
    public abstract void speak();
}
