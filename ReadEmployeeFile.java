import java.nio.file.*;
import java.io.*;

public class ReadEmployeeFile {

    public static void main(String[] args) {
        Path file = Paths.get("C:\\Users\\pcguest\\Desktop\\Daniel Temp\\Java 161\\Programs\\example.csv");

        String str = " ";

        try {
            InputStream input = new BufferedInputStream(Files.newInputStream(file));
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            str = reader.readLine();

            while (str != null) {
                System.out.println(str);
                str = reader.readLine();
            }
            reader.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}