import java.util.InputMismatchException;
import java.util.Scanner;

public class Division {
    public static void main(String[] args) {
        int numerator, denominator, result; 
        Scanner input = new Scanner(System.in);

        System.out.print("Enter numerator >> ");
        numerator = input.nextInt();
        
        System.out.print("Enter denominator >> ");
        denominator = input.nextInt();

        input.close();

        // This will test the math 
        try {
            result = numerator / denominator;
            System.out.println(numerator + "/" + denominator + "=" + result);
        } catch (ArithmeticException error) {
            System.out.println("You got bad math :" + error.toString());
        } catch (InputMismatchException error) {
            System.out.println("You don't know numbers : " + error.toString());
        }

        // Compare the structure of a try-catch block to an if-else statement. 
        // if (Boolean != false) {
            
        // } else {
            
        // }

    }
}