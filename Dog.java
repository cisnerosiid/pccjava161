/**
 * The dog class is a child class to the animal class.
 * 
 * @author Daniel Cisneros II
 * @version 1.0.0
 * @since 2/14/2019
 */
public class Dog extends Animal {
    private String says;

    public Dog() {
        super();
        this.says = "Bark";
    }

    public Dog(int weight, int legs, String says) {
        super(weight, legs);
        this.says = says; 
    }

    public void speak() {
        System.out.println(says);
    }

    public void eatFood(int food) {
        System.out.println("This method is being called from the Dog class");
        System.out.println("This dog ate " + food + " pounds");
    }
}