import java.util.Scanner;

public class EnumDemo {
    enum Month {
        JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC
    }

    public static void main(String[] args) {
        Month birthMonth;
        String userEntry;
        int position;
        int comparison; 
        Scanner input = new Scanner(System.in);

        //Print out the entire Month enum
        for(Month mon : Month.values() ){
            System.out.print(mon + " ");
        }

        // Query my user for their birth month and converts to uppercase.
        System.out.print("\nEnter the first three letters of your birth month>>");
        userEntry = input.nextLine().toUpperCase();
        birthMonth = Month.valueOf(userEntry);
        System.out.println("You Entered " + birthMonth);

        // Get the index for birthMonth.
        position = birthMonth.ordinal();
        System.out.println("So its month number " + (position + 1) );
 
        comparison = birthMonth.compareTo(Month.JUN);
        if(comparison < 0) {
            System.out.println(birthMonth + " is earlier in the year than " + Month.JUN);
        } else if (comparison > 0) {
            System.out.println(birthMonth + " is later in the year than " + Month.JUN);
        } else {
            System.out.println(birthMonth + " is " + Month.JUN);
        }

        input.close();
    } // End main()
} // End Class