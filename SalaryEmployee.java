public abstract class SalaryEmployee extends Employee {

    private double salary;

    public SalaryEmployee(String name, String address, int number, double salary) {
        super(name, address, number);
        this.salary = salary;
    }

    public abstract void mailCheck();

    public abstract double bonusCheck();

    public double computePay() {
        System.out.println("Computing salary pay for " + getName());
        return salary/52;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String toString() {
        return getName() + " " + getAddress() + " " + getNumber() + " " + getSalary();
    }
}