import java.nio.file.*;
import java.io.*;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;

public class WriteEmployeeFile {

    public static void main(String[] args) {
        // Objects that we are making.
        Scanner input = new Scanner(System.in);
        Path file = Paths.get("C:\\Users\\pcguest\\Desktop\\Daniel Temp\\Java 161\\Programs\\example.csv");

        // Variables used for records
        String str = " ";
        String delimiter = ",";
        int id;
        String name;
        double payRate;
        final int QUIT = 999;

        try {
            OutputStream output = new BufferedOutputStream(Files.newOutputStream(file, CREATE));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
            
            System.out.println("Enter an employee id or " + QUIT + " for quit >> ");
            id = input.nextInt();

            while (id != QUIT) {
                input.nextLine();
                System.out.println("Enter a name for the employee " + id + " >> ");
                name = input.nextLine();

                System.out.println("Enter a pay rate >> ");
                payRate = input.nextDouble();

                str = id + delimiter + name + delimiter + payRate;

                writer.write(str, 0, str.length());
                writer.newLine();
                writer.flush();

                System.out.println("Enter an employee id or " + QUIT + " for quit >> ");
                id = input.nextInt();    
            }
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            input.close();
        }
    }
}