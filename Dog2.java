/**
 * A subclass to the Animal2 class. (Child 1 of 2)
 * 
 * @author Daniel Cisneros II
 * @version 1.0
 * @since 2/21/2019
 */
public class Dog2 extends Animal2 {
    private String says;

    /**
     * Default Constructor
     */
    public Dog2() {
        this.says = "BARK!?";
    }
    
    /**
     * @return the says
     */
    public String getSays() {
        return says;
    }
    
    /**
     * @param says the says to set
     */
    public void setSays(String says) {
        this.says = says;
    }
    
    /**
     * What does the dog say. 
     */
    public void speak() {
        System.out.println("The dog says " + says);
    }
}