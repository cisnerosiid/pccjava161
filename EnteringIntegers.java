import java.util.Scanner;

public class EnteringIntegers {
    public static void main(String[] args) {
        int[] numberList = {0, 0, 0, 0, 0, 0, 0, 0,};
        int i; // Represents and array index.
        Scanner input = new Scanner(System.in);

        for(i = 0; i <numberList.length; i++) {
            try {
                System.out.print("Enter an Integer >> ");
                numberList[i] = input.nextInt();
            } catch (Exception error) {
                System.out.println("That is not an integer");
                i -= 1;
            }
            input.nextLine();
        }

        System.out.print("The numbers are: ");
        for (int value : numberList) {
            System.out.print(value + " ");
        }

        System.out.println();
    
    }
}