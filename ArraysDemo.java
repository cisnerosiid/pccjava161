import java.util.*;

public class ArraysDemo {
    public static void main(String[] args) {
        int[] myScores = new int[5];
        display("Original Array", myScores);

        Arrays.fill(myScores, 8);
        display("After filling with 8s:", myScores);

    } // End of main()

    // Create a method which takes in a message and an array then displays the
    // message and array.
    public static void display(String message, int array[]) {
        int size = array.length;
        System.out.println(message);
        for (int i = 0; i < size; ++i) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    } // End of display()
} // End of Class