// We are creating our first Interface.

// Implicity public and abstract you may find code missing those modifiers.
// This may be missing public and abstract but it still is public and abstract. 
public abstract interface Worker2 {
    
    // Declare Variables
    // All variables are static, public and final (Implicity)

    /**
     * This is the math element know as pie.
     */
    public final static double HALF_OF_PIE = 3.14/2 ;
    public final static int TOES = 10;

    // Declare Methods
    // All methods are public and abstract (Implicity)
    
    /**
     * A simple undefined method that does a little bit of work. 
     */
    public abstract void doMoreWork(); 
}