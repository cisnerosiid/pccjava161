/**
 * The MountainBike class extends the Bicycle class to show inheritance.
 * 
 * @author Daniel Cisneros II
 * @version 1.0.0
 * @since 2/7/2019
 */
public class MountainBike extends Bicycle {
    public int seatHeight;

    /**
     * The default constructor for the MountainBike class.
     * 
     * @param gear       Gear is the ratio of tension on the chains by number.
     * @param speed      Speed is the current acceleration.
     * @param seatHeight SeatHeight is the height of the seat on a mountain bike.
     */
    public MountainBike(int gear, int speed, int seatHeight) {
        super(gear, speed); // Super is used to access methods in the parent class.
        this.seatHeight = seatHeight;
    }

    /**
     * Sets the height of the mountain bike seat. 
     * 
     * @param seatHeight The new height of the seat in inches. 
     */
    public void setSeatHeight(int seatHeight) {
        this.seatHeight = seatHeight;
    }

    /**
     * Returns the current height of the seat.
     * 
     * @param seatHeight
     * @return The seat height in inches. 
     */
    public int getSeatHeight(int seatHeight) {
        return this.seatHeight;
    }

    /**
     * This method builds a string of the Mountain Bike gears, speed and seat height. 
     * 
     * @return String Returns the info in a string object. 
     */
    public String toString() {
        return (super.toString() + "\nThe seat height is " + this.seatHeight);
    }

} // End of Class