public class ArrayObject {
    public static void main(String[] args) {
        int [] myIntArray = {1, 3, 4, 5 }; // Unused Array
        Book[] bookArray = new Book[7];
        bookArray[0] = new Book("Daniel", "As Daniel Codes", "2019"); // How we instantiate that memory. 
        System.out.println("The title of the book is: " + bookArray[0].title);
        
        bookArray[0].title = "He learns new things.";
        System.out.println("The title of the book is: " + bookArray[0].title);
    } // End of main()
} // End of Class