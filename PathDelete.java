import java.nio.file.*;
import java.io.IOException;

public class PathDelete {
    public static void main(String[] args) {
        
        Path filePath = Paths.get("C:\\Users\\pcguest\\Desktop\\Daniel Temp\\Java 161\\Programs\\HopeNDreams.txt");
        
        try {
            Files.delete(filePath);
        } catch (NoSuchFileException e) {
            System.out.println("You deleted the file already or it does not exsist");
        } catch (DirectoryNotEmptyException e) {
            System.out.println("Your dir is not empty");
        } catch (SecurityException e) {
            System.out.println("You don't have the permissions to delete this file");
        } catch (IOException e) {
            System.out.println("IOException");
        }
    }
}