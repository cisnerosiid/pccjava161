import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.*;
import java.util.Scanner;
import static java.nio.file.StandardOpenOption.*;

public class ReadEmployeesRandom {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Path file = Paths.get("C:\\Users\\pcguest\\Desktop\\Daniel Temp\\Java 161\\Programs\\data.txt");
        String str = "0000,          ,00000.00" + System.getProperty("line.separator");
        final int RECSIZE = str.length();
        byte[] data = str.getBytes();
        ByteBuffer buffer = ByteBuffer.wrap(data);
        FileChannel filechannel = null;
        String idString;
        int id;
        final String QUIT = "999";

        try {
            filechannel = (FileChannel)Files.newByteChannel(file, READ, WRITE);
            System.out.println("Enter employe ID or " + QUIT + " to quit. >>");
            idString = input.nextLine();
            while(!idString.equals(QUIT)) {
                id = Integer.parseInt(idString);
                id = id - 1; //Great Programming
                buffer = ByteBuffer.wrap(data);
                filechannel.position(id * RECSIZE);
                filechannel.read(buffer);
                str = new String(data);
                System.out.println("ID # " + (id + 1) + " " + str);
                System.out.println("Enter employe ID or " + QUIT + " to quit. >>");
                idString = input.nextLine();
            }
            filechannel.close();
            input.close();
        } catch (Exception e) {
            System.out.println(e.getMessage() + " " + e.getStackTrace());
        } finally {
            System.out.println("You did it!");
        }
    }
}