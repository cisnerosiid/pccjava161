import java.nio.file.*;
import java.io.*;
import static java.nio.file.StandardOpenOption.*;

public class Output {
    public static void main(String[] args) {
        Path file = Paths.get("C:\\Users\\pcguest\\Desktop\\Daniel Temp\\Java 161\\Programs\\data.txt");

        String save = "ASDFGHJKLQWERTYUIOP";

        byte[] data = save.getBytes();
        OutputStream output = null;

        try {
            output = new BufferedOutputStream(Files.newOutputStream(file, CREATE));
            output.write(data);
            output.flush();
            output.close();            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}