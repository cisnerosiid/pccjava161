import java.util.Scanner;

public class UninitVar {
    public static void main(String[] args) {
        int x;
        Scanner input = new Scanner(System.in);

        try {
            System.out.println("Enter an integer >> ");
            x = input.nextInt();
        } catch (Exception e) {
            System.out.println("An exception has occurred");
            x = -1; 
        }
        System.out.println("The value of x is : " + x );

        input.close();
    }
}