public abstract class Employee {
    private String name;
    private String address;
    private int number; 

    public Employee(String name, String address, int number) {
        System.out.println("Constructing your employee");
        this.name = name;
        this.address = address;
        this.number = number;
    }

    public abstract double computePay();

    public abstract void mailCheck();

    public abstract String toString();

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public int getNumber() {
        return number;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}