public class LinkedList2 {
    Node head;
    Node tail;

    public static class Node {
        int data;
        Node next;
        Node prev;

        public Node(int data) {
            this.data = data;
            this.next = null;
            this.prev = null;
        }

        public void next(Node node) {
            this.next = node;
        }

        public void prev(Node node) {
            this.prev = node;
        }
    } // End Node Class

    public static void main(String[] args) {
        // Create a linked list to use head and tail pointers.
        LinkedList2 llist = new LinkedList2();

        // Create some starting nodes.
        Node three = new Node(3);
        Node six = new Node(6);
        Node nine = new Node(9);

        // Assign head and tail to our nodes.
        llist.head = three;
        llist.tail = nine;

        // Link the nodes together!
        three.next = six;
        six.next = nine;

        six.prev = three;
        nine.prev = six;

        llist.printAll("The start of your list");

        // Make a new node to test insert head
        Node one = new Node(1);
        llist.insertHead(one);
        llist.printAll("Your list after head changes");

        // Make a new node to test insert tail
        Node ten = new Node(10);
        llist.insertTail(ten);
        llist.printAll("Your list after tail changes");

        // Make a new node to test insert middle
        Node seven = new Node(7);
        llist.insertMiddle(seven, six);
        llist.printAll("Your list after insert middle");

        // Delete the head of a linked list.
        llist.deleteHead();
        llist.printAll("Your list after deleting head");

        // Delete the tail of a linked list.
        llist.deleteTail();
        llist.printAll("Your list after deleting tail");

        // Delete from the middle of the list.
        llist.deleteMiddle(seven);
        llist.printAll("Your list after deleting the middle");

    } // End Main 

    /**
     * Deletes a node in the middle of the list.
     * 
     * @param deleteNode the node to be deleted
     */
    public void deleteMiddle(Node deleteNode){
        Node head = this.head;
        while (head != deleteNode) {
            head = head.next;
        }
        Node nodeBefore = head.prev;
        Node nodeAfter = head.next;
        nodeBefore.next = nodeAfter;
        nodeAfter.prev = nodeBefore;
    }

    /**
     * Deletes the tail of the linked list.
     */
    public void deleteTail() {
        this.tail = this.tail.prev;
        this.tail.next = null;
    }

    /**
     * Deletes the head of the linked list.
     */
    public void deleteHead() {
        this.head = this.head.next;
        this.head.prev = null;
    }

    /**
     * Insert a node in the middle of a linked list.
     * 
     * @param newNode
     * @param nodeBefore
     */
    public void insertMiddle(Node newNode, Node nodeBefore) {
        Node head = this.head;
        while (head != nodeBefore) {
            head = head.next;
        }
        Node nodeAfter = nodeBefore.next;
        newNode.prev = nodeBefore;
        newNode.next = nodeAfter;
        nodeBefore.next = newNode;
        nodeAfter.prev = newNode;
    }

    /**
     * Insert a node at the head of a linked list.
     * 
     * @param newNode the node to be inserted
     */
    public void insertHead(Node newNode) {
        newNode.next = this.head;
        this.head.prev = newNode;
        this.head = newNode;
        this.head.prev = null;
    }

    /**
     * Insert a node at the tail of a linked list.
     * 
     * @param newNode the node to be inserted
     */
    public void insertTail(Node newNode) {
        this.tail.next = newNode;
        newNode.prev = this.tail;
        this.tail = newNode;
        this.tail.next = null;
    }

    /**
     * Prints the linked list from the head.
     */
    public void printList() {
        Node head = this.head;
        while (head != null) {
            System.out.print(head.data + " ");
            head = head.next;
        }
        System.out.println();
    }

    /**
     * Print the linked list from the tail.
     */
    public void printListReverse() {
        Node tail = this.tail;
        while (tail != null) {
            System.out.print(tail.data + " ");
            tail = tail.prev;
        }
        System.out.println();
    }

    /**
     * Print the linked list forwards and backwards.
     * 
     * @param message the message to include with the printing
     */
    public void printAll(String message) {
        System.out.println(message);
        this.printList();
        System.out.println(message + " but in reverse");
        this.printListReverse();
    }
} // End LinkedList2 Class