/**
 * The cow class is a child class to the animal class. (Child 2 of 2)
 * 
 * @author Daniel Cisneros II
 * @version 1.0
 * @since 2/21/2019
 */
public class Cow2 extends Animal2 {
    private String says;

    public Cow2() {
        this.says = "MOOOOOO...";
    }

    /**
     * What the cow says.
     * 
     * @return the says
     */
    public String getSays() {
        return says;
    }

    /**
     * What the cow says.
     * 
     * @param says the says to set
     */
    public void setSays(String says) {
        this.says = says;
    }

    public void speak() {
        System.out.println("The cow says " + says);
    }
}
