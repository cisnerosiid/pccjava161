public class TestArray {
    public static void main(String[] args) {

        // Create a double array.
        double[] myList = {1.9, 2.9, 3.4, 3.5};

        // Print all the array elements.
        for (int index = 0; index < myList.length; index++) {
            System.out.print(myList[index] + " "); // 1.9 2.9 3.4 3.5
        }
        
        // Summing all elements. Make a total.
        double total = 0;
        for (int index = 0; index < myList.length; index++) {
            total += myList[index]; // Also means total = total + myList[index];
        }
        System.out.println("\nTotal is " + total); // 11.7
        
        // Finding the largest element.
        double max = myList[0]; // 1.9 at the start. 
        for (int index = 0; index < myList.length; index++) {
            if (myList[index] > max) {
                max = myList[index];
            }
        }
        System.out.println("Max number is " + max); // 3.5
        
    } // End Main()
} // End Class