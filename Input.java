import java.nio.file.*;
import java.io.*;

public class Input {
    public static void main(String[] args) {
        Path file = Paths.get("C:\\Users\\pcguest\\Desktop\\Daniel Temp\\Java 161\\Programs\\data.txt");

        InputStream input = null;

        try {
            input = Files.newInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String str = null;
            str = reader.readLine();

            System.out.print(str);
            input.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}