import java.nio.file.*;
import static java.nio.file.AccessMode.*;
import java.io.IOException;

public class FileAccess {
    public static void main(String[] args) {
        
        Path filePath = Paths.get("C:\\Users\\pcguest\\Desktop\\Daniel Temp\\Java 161\\Programs\\data.txt");

        System.out.println("Path is " + filePath.toString());

        try {
            filePath.getFileSystem().provider().checkAccess(filePath, READ, EXECUTE);
            System.out.println("This file works with your application");
        } catch (IOException e) {
            System.out.println("Your application can't work with this file.");
        }
    }
}