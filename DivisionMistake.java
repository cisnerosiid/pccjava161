import java.util.*;

public class DivisionMistake {
    public static void main(String[] args) {
        int numerator, denominator, result;
        Scanner input = new Scanner(System.in);
        double accountBalance = 1000.00;
        double tempBalance = 0;

        try {
            System.out.println("Enter the numerator >> " );
            numerator = input.nextInt();

            System.out.println("Enter the denominator >> " );
            denominator = input.nextInt();

            result = numerator / denominator; 
            System.out.println(numerator + " / " + denominator + " = " + result );

            tempBalance = accountBalance;

            // crazy math to tempBalance
            tempBalance = tempBalance / denominator;

        } catch (InputMismatchException e) {
            System.out.println("You handled an InputMismatchException");
            tempBalance = accountBalance;
        } catch (ArithmeticException e) {
            System.out.println("You handled an ArithmeticException");
            tempBalance = accountBalance;
        } finally { 
            input.close();
            System.out.println("You are at the end. Have a great Spring Break!");
            accountBalance = tempBalance;
        }
        System.out.println(tempBalance);

    }
}