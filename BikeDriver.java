// The driver class runs all the other classes and contains the main()

public class BikeDriver {
    public static void main(String[] args) {
        Bicycle myBike = new Bicycle(6, 20);

        System.out.println(myBike.toString());
        myBike.speedUp(5);

        System.out.println(myBike.toString());

        System.out.println("**********************");

        MountainBike myMountainBike = new MountainBike(4, 100, 3);

        System.out.println(myMountainBike.toString());

        Bicycle myJavaDOCBike = new Bicycle();

        myJavaDOCBike.applyBrake(10);

        myMountainBike.applyBrake(99);
    }
} // End of Class