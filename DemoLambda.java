
public class DemoLambda {
    public static void main(String[] args) {
        int pounds;

        pounds = 15; 

        // A simple Lambda expression that dosen't need a complete method. (anonymous)

        Worker washingMachine = () -> {
            //Add multiple statements 
            System.out.println("I get clothes clean says the washer.");
            System.out.println("My washer cleans " + pounds + " pounds of clothes");
        };

        washingMachine.doWork();
    }
}