public class MultiArray {
    public static void main(String[] args) {
        // Declare a 2D array.
        int arr[][] = {
            {  2  ,  7  ,  9  },
            {  3  ,  6  ,  1  },
            {  7  ,  4  ,  2  }
        };

        // Printing 2D array
        for (int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println(arr[2][1]);

    } // End of main()

} // End of Class