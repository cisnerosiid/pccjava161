
/**
 * LinkedList
 */
public class LinkedList {
    Node head;

    static class Node {
        int data; 
        Node next; 

        //Build a constructor
        public Node(int data) {
            this.data = data;
            this.next = null;
        }
    }

    public static void main(String[] args) {
        LinkedList llist = new LinkedList();

        llist.head = new Node(2);
        Node second = new Node(5);
        Node third = new Node(10);

        llist.head.next = second;
        second.next = third;

        System.out.println("The start of our linked list.");
        llist.printList();

        // Insert a node at the beginning of the list.
        Node newNode = new Node(1);
        insertHead(newNode, llist);
        
        // Insert a node in the middle 
        Node middleNode = new Node(6);
        insertMiddle(middleNode, second, llist);
        
        // Insert a node at the end of the list. 
        Node lastNode = new Node(11);
        insertEnd(lastNode, llist);
        
        System.out.println("The linked list after the insert.");
        llist.printList();

        // Deletes the head
        deleteHead(llist);

        // Delete a middle node.
        deleteMiddle(third, llist);

        // Delete a node at the end of the list.
        deleteEnd(llist);

        System.out.println("The linked list after the deletes");
        llist.printList();

    }

    /**
     * Inserts a node into the front of the linked list
     * and moves head to point to it.
     * 
     * @param newNode the node to be inserted
     * @param llist the targeted linked list
     */
    public static void insertHead(Node newNode, LinkedList llist) {
        newNode.next = llist.head;
        llist.head = newNode;
    }

    /**
     * Inserts a node in the middle of a linked listed when given
     * the node that goes before it.
     * 
     * @param newNode the node to be inserted.
     * @param nodeBefore the node before the inserted node.
     */
    public static void insertMiddle(Node newNode, Node nodeBefore, LinkedList llist) {
        Node head = llist.head;
        while (head.next != nodeBefore.next) {
            head = head.next;
        }
        newNode.next = nodeBefore.next;
        nodeBefore.next = newNode;
    }

    /**
     * Inserts a node at the end of a linked list.
     * 
     * @param newNode the node to be inserted.
     * @param llist the linked list to be targeted. 
     */
    public static void insertEnd(Node newNode, LinkedList llist) {
        Node head = llist.head;
        while (head.next != null) {
            head = head.next;
        }
        head.next = newNode;
    }

    /**
     * Deletes the head of a given linked list.
     * 
     * @param llist the linked list to be targeted. 
     */
    public static void deleteHead(LinkedList llist) {
        Node currentHead = llist.head;
        Node newHead = llist.head.next;
        llist.head = newHead;
        currentHead.next = null;
    }

    /**
     * Deletes the given node from the given linked list.
     * 
     * @param deleteNode node to be deleted.
     * @param llist targeted linked list to change. 
     */
    public static void deleteMiddle(Node deleteNode, LinkedList llist) {
        Node head = llist.head;
        while(head.next != deleteNode) {
            head = head.next;
        }
        head.next = deleteNode.next;
        deleteNode.next = null;
    }

    /**
     * Deletes the node at the end of the list
     * 
     * @param llist the linked list to be targeted. 
     */
    public static void deleteEnd(LinkedList llist) {
        Node head = llist.head;
        Node newEnd = null;
        while(head.next != null) {
            newEnd = head;
            head = head.next;
        }
        newEnd.next = null;
    }

    public void printList() {
        Node n = head;
        while (n != null) {
            System.out.print(n.data + " ");
            n = n.next;
        }
        System.out.println();
    }

}