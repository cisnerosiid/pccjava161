import java.nio.file.*;
import java.nio.file.attribute.*;
import java.io.IOException;

public class FileAttributes {
    public static void main(String[] args) {
        Path filePath = Paths.get("C:\\Users\\pcguest\\Desktop\\Daniel Temp\\Java 161\\Programs\\data.txt");

        try {
            BasicFileAttributes attr = Files.readAttributes(filePath, BasicFileAttributes.class);

            System.out.println(attr.creationTime());
            System.out.println(attr.lastAccessTime());
            System.out.println(attr.size() * .001 + "kb");

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}