/**
 * The Bicycle class is a base class to demonstrate inheritance.
 * 
 * @author Daniel Cisneros II
 * @version 1.0.0
 */
public class Bicycle {
    /**
     * The current gear of the bicycle. 
     */
    private int gear;
    private int speed;

    /**
     * The default constructor for the Bicycle class. The gear is set to 16 and the
     * speed is set to 9.
     */
    public Bicycle() {
        this.gear = 16;
        this.speed = 9;
    }

    /**
     * Alternative constructor for the Bicycle class.
     * 
     * @param gear  Gear is the ratio of tension on the chains by number.
     * @param speed Speed is the current acceleration.
     */
    public Bicycle(int gear, int speed) {
        this.gear = gear;
        this.speed = speed;
    }

    /**
     * This method decreases the speed of the Bicycle. If the speed drops below 0
     * then the speed is 0.
     * 
     * @param decrement Decrements the amount of speed of the Bicycle.
     */
    protected void applyBrake(int decrement) {
        speed -= decrement;

        if (speed < 0) {
            speed = 0;
        }
    }

    /**
     * This method increases the speed of the Bicycle. There is no LIMITS!
     * 
     * @param increment Increments the amount of speed of the Bicycle.
     */
    public void speedUp(int increment) {
        speed += increment;
    }

    /**
     * This method builds a string of the Bicycle gears and speed. 
     * 
     * @return String Returns the info in a string object. 
     */
    public String toString() {
        return ("Number of gears are " + gear + "\n" + "speed of bicycle is " + speed);
    }

} // End of Class