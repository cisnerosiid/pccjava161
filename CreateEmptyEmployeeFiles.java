import java.nio.file.*;
import java.io.*;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;

public class CreateEmptyEmployeeFiles {

    public static void main(String[] args) {
        Path file = Paths.get("C:\\Users\\pcguest\\Desktop\\Daniel Temp\\Java 161\\Programs\\data.txt");
        String str = "0000,          ,000000.00" + System.getProperty("line.separator");
        byte[] data = str.getBytes();
        ByteBuffer buffer = ByteBuffer.wrap(data);
        final int NUMRECS = 1000;

        try {
            OutputStream output = new BufferedOutputStream(Files.newOutputStream(file, CREATE));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
            for(int count = 0; count < NUMRECS; count++) {
                writer.write(str, 0, str.length());
            }
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}