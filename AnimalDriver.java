public class AnimalDriver {
    public static void main(String[] args) {

        Animal bear = new Animal(70, 4);

        bear.setLegs(5);

        bear.getWeight();

        Dog apollo = new Dog(45, 4, "WOOF");

        apollo.eatFood(10);

    } // End of main()
} // End of Class