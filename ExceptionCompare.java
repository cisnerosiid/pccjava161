import java.nio.channels.InterruptedByTimeoutException;

import com.sun.corba.se.impl.logging.InterceptorsSystemException;
import com.sun.xml.internal.ws.wsdl.parser.InaccessibleWSDLException;

import sun.management.counter.perf.InstrumentationException;

public class ExceptionCompare {
    static String result = "";

    public static void main(String[] args) {

        methodA();
        if (result.startsWith("A")) {
            methodB();
            if (result.startsWith("B")) {
                methodC();
                if (result.startsWith("C")) {
                    System.out.println("Your program has run successfully");
                } else {
                    System.out.println("C method has failed");
                }
            } else {
                System.out.println("B method has failed.");
            }
        } else {
            System.out.println("A method has failed.");
        }

        try {
            methodA();
            methodB();
            methodC();
            System.out.println("Your program has run successfully");
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
        }

    }

    public static void methodA() throws InaccessibleWSDLException, InstrumentationException{
        result = "A Succeeded";
    }

    public static void methodB() {
        result = "B Succeeded";
    }

    public static void methodC() {
        result = "C Succeeded";
    }

}